//
// Written by Jeff Biggs, Simperative Technologies LLC
//
// Copyright (C) 2012  Jeff Biggs, jeff.biggs@simperative.com
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation; either version 2 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
//
// $Id$

#ifndef __OsgEarthModelLoad_h__
#define __OsgEarthModelLoad_h__

#include <osg/Referenced>
#include <osg/ref_ptr>
#include <osg/MatrixTransform>
#include <osg/observer_ptr>
#include <osgEarth/MapNode>
#include <osgEarth/IOTypes>

namespace simgear {

class OsgEarthModelLoad : public osg::Referenced
{

public:

    OsgEarthModelLoad() 
    :   osg::Referenced(),
        m_InputPath("")
    {}
    
    ~OsgEarthModelLoad() {}


public:

    // singleton access
    static OsgEarthModelLoad* Instance();

public:
    // load KML models from a directory
    osg::Group* LoadKMLmodels(const std::string& path);

    // return file input path 
    std::string GetInputPath() { return m_InputPath; }

private:
    // singleton instance
    static osg::ref_ptr<OsgEarthModelLoad> s_Instance;

protected:
    std::string m_InputPath;
};

}

#endif