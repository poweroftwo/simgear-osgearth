//
// Written by Jeff Biggs, Simperative Technologies LLC
//
// Copyright (C) 2014 Jeff Biggs, jeff.biggs@simperative.com
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation; either version 2 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
//
// $Id$

#ifndef __OsgEarthUtil_h__
#define __OsgEarthUtil_h__


#include <osg/Node>

// ensure that OsgEarth is marked as ready once all input processing is complete
void monitorIsEarthReady();

// return true if osgEarth is enabled and ready to render
bool isEarthReady();

// load KML models from file path and places them into the osgEarth scene
void loadKmlModels(const char* name);

// return osgEarth scene graph
osg::Node* GetEarthScene();

// assign osgEarth scene graph
void setEarthScene(osg::Node* node);

#endif